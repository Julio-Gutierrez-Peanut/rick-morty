var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('./config');

exports.create = (user) => {
  var payload = {
    sub: user.username,
    iat: moment().unix(),
    exp: moment().add(2, "minutes").unix(),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};
