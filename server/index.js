const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const requestApi = require('request');

const redis = require('redis');
const client = redis.createClient(6379, 'redis');
client.set('admin', JSON.stringify({username: 'admin', password: '1234'}));

const authorization = require('./security/authorization');
const serviceToken = require('./security/token');

const DIST_DIR = path.join(__dirname, '../dist');
const HTML_FILE = path.join(DIST_DIR, 'index.html');

const app = express();
const router = express.Router();

const port = process.env.PORT || 3000;

app.use(express.static(DIST_DIR));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(router);

router.get('', (request, response) => {
  response.send(HTML_FILE);
});

router.post('/token', (request, response) => {
  var username = request.body.user
  var password = request.body.password
  
  client.get(username, (error, result) => {
    if(result == null || JSON.parse(result).password !== password) {
      response.status(401).send({
        error: 'usuario o contraseña inválidos'
      });
    } else {
      response.status(200).send({token: serviceToken.create({username})});
    }
  });
});

router.get('/characters', authorization.verify, (request, response) => {
  requestApi({
    uri: 'https://rickandmortyapi.com/api/character/'
  }).pipe(response);
});

http.createServer(app).listen(port, () => {
  console.log('Server started at http://localhost:3000');
});
