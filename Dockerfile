FROM node:latest
WORKDIR /usr/rick-morty
COPY ./package*.json ./
RUN npm install --no-optional && npm cache clean --force
COPY ./ ./