import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import Sign from './features/sign/sign';
import Home from './features/home/home';

class Index extends Component {
  render() {
    return(
      <Router>
        <Route exact path='/' render={()=> <Sign />}></Route>
        <Route exact path='/home' render={() => <Home />}></Route>
      </Router>      
    );
  }
}

ReactDOM.render(<Index />, document.getElementById('root'));
