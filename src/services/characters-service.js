import Axios from "axios";

const GetCharacters = () => {
  const config = {
    headers: {
      Authorization: 'bearer ' + localStorage.getItem('token')
    }
  }

  return Axios.get('http://localhost:3000/characters', config);
}

export default GetCharacters;
