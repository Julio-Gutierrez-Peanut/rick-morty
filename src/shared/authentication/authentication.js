import axios from 'axios';
import querystring from 'querystring';

export default function Authenticate(user) {
  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    } 
  };
  let body = querystring.stringify({user: user.username, password: user.password});
  
  return axios.post('http://localhost:3000/token', body, config);
}
