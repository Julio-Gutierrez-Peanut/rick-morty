import React from "react";
import { Card, CardContent } from "@material-ui/core";

export default class CharacterCard extends React.Component {
  render() {
    return (
      <Card style={{ marginBottom: '10px', marginTop: '10px' }}>
        <CardContent>
          <div>
            <img src={this.props.image} alt="" style={{ width: '100%' }} />
          </div>
          <br/>
          <ul>
            <li>Name: {this.props.name}</li>
            <li>Status: {this.props.status}</li>
            <li>Specie: {this.props.species}</li>
            <li>Gender: {this.props.gender}</li>
          </ul>
        </CardContent>
      </Card>
    );
  }
}