import * as React from "react";
import GetCharacters from "../../services/characters-service";
import CharacterCard from '../../shared/items/card-character';
import { Col, Container, Row } from "react-bootstrap";

export default class Home extends React.Component {
  results = [];

  state = {
    resultsState: this.results
  }

  constructor(props) {
    super(props);

    GetCharacters()
      .then(response => {
        this.setState({
          resultsState: response.data.results
        });

        console.log(this.state.resultsState);
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { resultsState } = this.state;
    const cards = resultsState.map((item, key) =>
      <Col md='3' key={key}>
        <CharacterCard
          id={item.id}
          name={item.name}
          type={item.type}
          gender={item.gender}
          image={item.image}
          species={item.species}
          status={item.status} />
      </Col>
    );

    return (
      <Container style={{marginTop: '2.5%'}}>
        <Row>
          <Col style={{textAlign: 'center'}}>
            <h1>RICK AND MORTY CHARACTERS</h1>
          </Col>
        </Row>
        <Row>
          {cards}
        </Row>
      </Container>
    );
  }
}
