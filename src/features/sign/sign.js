import React, { Component } from 'react';
import { Card, CardContent, CardActions, TextField, Button, CardHeader } from '@material-ui/core';
import { Container, Row, Col } from 'react-bootstrap';
import './sign.css';

import Authenticate from '../../shared/authentication/authentication';
import { Redirect } from 'react-router-dom';

export default class Sign extends Component {
  state = {
    username: '',
    password: '',
    redirect: false
  }

  handleSignIn() {
    Authenticate(this.state)
      .then(response => {        
        localStorage.setItem('token', response.data.token);
        this.setState({ username: '', password: '', redirect: true });
      })
      .catch(error => {
        console.log(error);
      });
  }

  _handleUserValue = (event) => {
    this.setState({
      username: event.target.value
    });
  }

  _handlePasswordValue = (event) => {
    this.setState({
      password: event.target.value
    });
  }

  render() {
    const { redirect } = this.state;

    if(redirect) {
        return <Redirect to='/home' />
    }

    return (
      <Container style={{ marginTop: '25px' }}>
        <Row style={{ marginBottom: '25px' }}>
          <Col md='12' style={{ textAlign: 'center' }}>
            <h2>LOGIN</h2>
          </Col>
        </Row>
        <Row>
          <Col md='4' />
          <Col md='4'>
            <Card>
              <CardContent>
                <form>
                  <TextField
                    label="Email"
                    type="email"
                    placeholder="a@a.com"
                    className="full-width"
                    value={this.state.username}
                    onChange={this._handleUserValue}
                  />
                  <TextField
                    label="Password"
                    type="password"
                    className="full-width"
                    value={this.state.password}
                    onChange={this._handlePasswordValue} />
                </form>
              </CardContent>
              <CardActions>
                <Button
                  variant="contained"
                  color="primary"
                  className="full-width"
                  onClick={() => this.handleSignIn()}
                >
                  Sign In
                </Button>
              </CardActions>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
